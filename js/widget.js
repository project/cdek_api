/**
 * @file
 * Defines Javascript behaviors for a 'cdek_widget' element.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.cdekApi = Drupal.cdekApi || {};
  Drupal.cdekApi.widgets = {};

  if (drupalSettings.cdekApi && drupalSettings.cdekApi.widgetApiUrl) {
    var script = document.createElement('script');

    script.src = drupalSettings.cdekApi.widgetApiUrl;
    script.id = 'ISDEKscript';

    script.onload = function () {
      $(window).trigger('cdekApiWidgetApiReady');
    };

    document.head.append(script);
  }

  Drupal.behaviors.cdekApiWidget = {
    attach: function (context, settings) {
      if (settings && settings.cdekApi && settings.cdekApi.widgets) {
        var widgets = settings.cdekApi.widgets;

        if (typeof ISDEKWidjet === 'function') {
          Drupal.cdekApi.processWidgets(context, widgets);
        }
        else {
          $(window).on('cdekApiWidgetApiReady', {context: context, widgets: widgets}, function (event) {
            Drupal.cdekApi.processWidgets(event.data.context, event.data.widgets);
          });
        }
      }
    }
  };

  Drupal.cdekApi.processWidgets = function (context, widgets) {
    Object.keys(widgets).forEach(function (id) {
      var selector = '.js-cdek-widget-' + id;
      var $element = $(context).find(selector).once('cdek-api-widget');

      if ($element.length) {
        Drupal.cdekApi.widgets[id] = new Drupal.cdekApi.widget($element, widgets[id]);
      }
    });
  };

  Drupal.cdekApi.widget = function ($element, settings) {
    var options = $.extend({}, settings.options, {
      onReady: $.proxy(this.onReady, this),
      onCalculate: $.proxy(this.onCalculate, this),
      onChoose: $.proxy(this.onChoose, this),
      onChooseProfile: $.proxy(this.onChooseProfile, this),
    });

    this.$element = $element;
    this.settings = settings;
    this.widget = new ISDEKWidjet(options);

    $element.find('.cdek-widget-popup').on('click', $.proxy(this.onPopupClick, this));
  };

  Drupal.cdekApi.widget.prototype.onReady = function () {
    this.$element.find('button').prop('type', 'button');
    this.$element.trigger('cdekApiWidgetReady', [this]);
  };

  Drupal.cdekApi.widget.prototype.onCalculate = function (data) {
    this.$element.trigger('cdekApiWidgetDeliveryCalculated', [this, data]);
  };

  Drupal.cdekApi.widget.prototype.onChoose = function (data) {
    this.$element.find('.cdek-widget-value').val(data.id);
    this.$element.find('.cdek-widget-item').text(data.PVZ.Name);
    this.$element.trigger('cdekApiWidgetPointSelected', [this, data]);
  };

  Drupal.cdekApi.widget.prototype.onChooseProfile = function (data) {
    this.$element.trigger('cdekApiWidgetCourierSelected', [this, data]);
  };

  Drupal.cdekApi.widget.prototype.onPopupClick = function (event) {
    event.preventDefault();
    this.widget.open();
  };
})(jQuery, Drupal, drupalSettings);
