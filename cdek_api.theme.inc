<?php

/**
 * @file
 * Theme functions for the CDEK API module.
 */

/**
 * Prepares variables for cdek_select templates.
 *
 * Default template: cdek-select.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function template_preprocess_cdek_select(array &$variables) {
  $element = $variables['element'];
  $variables['attributes'] = [];

  if (isset($element['#id'])) {
    $variables['attributes']['id'] = $element['#id'];
  }
  $variables['children'] = $element['#children'];
}

/**
 * Prepares variables for cdek_widget templates.
 *
 * Default template: cdek-widget.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - element: An associative array containing the properties of the element.
 */
function template_preprocess_cdek_widget(array &$variables) {
  $element = $variables['element'];

  if (isset($element['#id'])) {
    $variables['attributes']['id'] = $element['#id'];
  }
  $variables['processed'] = !empty($element['#processed']);
  $variables['widget_id'] = $element['#cdek_widget_id'];
  $variables['children'] = $element['#children'];
}
