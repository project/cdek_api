INTRODUCTION
------------

This module provides basic integration with the CDEK Delivery Service API.
Please visit the CDEK site (https://www.cdek.ru) for more information about the
service. This module won't do much by itself - it is intended for developers.


FEATURES
--------

* Getting reference data.
* Cost of delivery calculation.
* Order management.
* Reference data can be cached to improve performance.
* Form element for selecting the pickup point.
* Delivery type selection widget (https://widget.cdek.ru).
* Test mode.


REQUIREMENTS
------------

This module requires:

 * Drupal 8.8 or greater.
 * CDEK SDK - https://github.com/sanmai/cdek-sdk


INSTALLATION
------------

Use Composer to install the module. Visit:
https://www.drupal.org/docs/develop/using-composer
for further information.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Administer CDEK API

     Users in roles with the "Administer CDEK API" permission can perform
     administration tasks for CDEK API module.

 * Configure the module settings in Administration » Configuration »
   Web services » CDEK API settings.


CREDITS / CONTACT
-----------------

Developed and maintained by Andrey Tymchuk (WalkingDexter)
https://www.drupal.org/u/walkingdexter

Ongoing development is sponsored by Drupal Coder.
https://www.drupal.org/drupal-coder-initlab-llc
