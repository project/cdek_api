<?php

namespace Drupal\cdek_api\Element;

use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Utility\Crypt;

/**
 * Provides a form element for pickup points widget.
 *
 * Note: Although this extends FormElement, it can be used outside the
 * context of a form.
 *
 * @FormElement("cdek_widget")
 */
class CdekWidget extends FormElement {

  /**
   * JavaScript API.
   */
  const API_URL = 'https://widget.cdek.ru/widget/widjet.js';

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#process' => [[$class, 'processCdekWidget']],
      '#pre_render' => [[$class, 'preRenderCdekWidget']],
      '#theme_wrappers' => ['cdek_widget'],
      '#cdek_widget_options' => [],
      '#cdek_widget_tariff_list' => [],
      '#cdek_widget_popup_title' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $value = '';

    if ($input === FALSE) {
      if (isset($element['#default_value'])) {
        $point = static::cdekApi()->getPickupPoint($element['#default_value']);

        if ($point !== NULL) {
          $value = $point->Code;
        }
      }
    }
    elseif (is_scalar($input)) {
      $value = $input;
    }
    return $value;
  }

  /**
   * Processes a cdek_widget form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processCdekWidget(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $element['#theme_wrappers'][] = 'form_element';
    $element['#title_display'] = 'none';

    $element['item'] = [
      '#type' => 'item',
      '#required' => $element['#required'],
      '#input' => FALSE,
    ];
    $element['item']['item'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'class' => ['cdek-widget-item'],
      ],
    ];

    $element['value'] = [
      '#type' => 'hidden',
      '#parents' => $element['#parents'],
      '#attributes' => [
        'class' => ['cdek-widget-value'],
      ],
    ];

    if (isset($element['#title'])) {
      $element['item']['#title'] = $element['#title'];
    }
    if (!empty($element['#value'])) {
      $point = static::cdekApi()->getPickupPoint($element['#value']);

      if ($point !== NULL) {
        $element['item']['item']['#value'] = $point->Name;
        $element['value']['#default_value'] = $point->Code;
      }
    }
    return $element;
  }

  /**
   * Pre-render callback.
   *
   * @param array $element
   *   The element structure.
   *
   * @return array
   *   The element structure.
   */
  public static function preRenderCdekWidget(array $element) {
    if (empty($element['#cdek_widget_id'])) {
      $element['#cdek_widget_id'] = Crypt::randomBytesBase64();
    }
    $widget_id = $element['#cdek_widget_id'];
    $tariff_list = $element['#cdek_widget_tariff_list'];
    $options = static::buildOptions($element);

    // Store tariffs in the cache for later use in the widget controller.
    // @see \Drupal\cdek_api\Controller\WidgetController::doActionCalc()
    if (!empty($tariff_list)) {
      $cid = 'tariff_list:' . $widget_id;
      $expire = \Drupal::time()->getRequestTime() + 21600;
      static::cache()->set($cid, $tariff_list, $expire);
    }

    // When the widget is considered popup.
    if ($options['popup']) {
      $element['popup'] = [
        '#type' => 'link',
        '#title' => $element['#cdek_widget_popup_title'] ?? t('Show widget'),
        '#url' => Url::fromRoute('<none>'),
        '#attributes' => [
          'class' => ['cdek-widget-popup'],
        ],
      ];
    }

    // When the widget is considered static.
    if ($options['link']) {
      $element['link'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#attributes' => [
          'id' => $options['link'],
          'class' => ['cdek-widget-link'],
        ],
      ];
    }

    // Library to process the widget.
    $element['#attached']['library'][] = 'cdek_api/widget';
    $element['#attached']['drupalSettings']['cdekApi']['widgetApiUrl'] = static::API_URL;
    $element['#attached']['drupalSettings']['cdekApi']['widgets'][$widget_id] = [
      'id' => $widget_id,
      'options' => $options,
    ];

    // Disable element caching.
    $element['#cache']['max-age'] = 0;
    return $element;
  }

  /**
   * Builds the widget options.
   *
   * @param array $element
   *   The element structure.
   *
   * @return array
   *   The widget options.
   */
  protected static function buildOptions(array $element) {
    $options = $element['#cdek_widget_options'];
    $widget_id = $element['#cdek_widget_id'];

    // Merge the default options.
    $options += [
      'hideMessages' => TRUE,
      'detailAddress' => FALSE,
      'popup' => FALSE,
    ];

    $options['link'] = $options['popup'] ? FALSE : 'cdek-widget-link-' . $widget_id;
    $options['servicepath'] = Url::fromRoute('cdek_api.widget.action', ['widget_id' => $widget_id])->toString();

    if (empty($element['#processed'])) {
      $options['choose'] = FALSE;
    }
    else {
      $options['hidedelt'] = TRUE;
      $options['choose'] = TRUE;
    }
    return $options;
  }

  /**
   * Gets the cdek_api service.
   *
   * @return \Drupal\cdek_api\CdekApi
   *   The cdek_api service.
   */
  protected static function cdekApi() {
    return \Drupal::service('cdek_api');
  }

  /**
   * Gets the cache object.
   *
   * @return \Drupal\Core\Cache\CacheBackendInterface
   *   The cache object associated with the 'cdek_api' bin.
   */
  protected static function cache() {
    return \Drupal::cache('cdek_api');
  }

}
