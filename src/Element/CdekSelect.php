<?php

namespace Drupal\cdek_api\Element;

use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Render\Element\CompositeFormElementTrait;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Render\Element;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Html;
use CdekSDK\Requests\PvzListRequest;

/**
 * Provides a form element for selecting the pickup point.
 *
 * @FormElement("cdek_select")
 */
class CdekSelect extends FormElement {

  use CompositeFormElementTrait;

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#tree' => TRUE,
      '#process' => [[$class, 'processCdekSelect']],
      '#after_build' => [[$class, 'afterBuildCdekSelect']],
      '#pre_render' => [[$class, 'preRenderCompositeFormElement']],
      '#theme_wrappers' => ['cdek_select'],
      '#cdek_request' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    $value = [];

    if ($input === FALSE) {
      if (isset($element['#default_value'])) {
        $point = static::cdekApi()->getPickupPoint($element['#default_value']);

        if ($point !== NULL) {
          $value = [
            'country' => $point->CountryCode,
            'region' => $point->RegionCode,
            'city' => $point->CityCode,
            'point' => $point->Code,
          ];
        }
      }
    }
    elseif (is_array($input)) {
      $value = $input;
    }
    return $value;
  }

  /**
   * Processes a cdek_select form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   */
  public static function processCdekSelect(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $value = is_array($element['#value']) ? $element['#value'] : [];
    $value += array_fill_keys(['country', 'region', 'city', 'point'], '');
    $is_value_locked = FALSE;

    $request = static::resolveRequest($element);
    $params = $request->getParams();

    $hide_city = isset($params['cityid']);
    $hide_region = isset($params['regionid']) || $hide_city;
    $hide_country = isset($params['countryid']) || $hide_region;

    // Generate a unique wrapper HTML ID.
    $ajax_wrapper_id = Html::getUniqueId('ajax-wrapper');
    $ajax_settings = [
      'callback' => [get_called_class(), 'ajaxUpdateCallback'],
      'options' => [
        'query' => [
          'element_parents' => implode('/', $element['#array_parents']),
        ],
      ],
      'wrapper' => $ajax_wrapper_id,
      'effect' => 'fade',
    ];

    // Element to select the country.
    $element['country'] = $element['country'] ?? [];
    $element['country'] = static::processNestedElement($element['country'], [
      '#type' => 'select',
      '#title' => t('Country'),
      '#ajax' => $ajax_settings,
      '#access' => !$hide_country,
      '#weight' => 0,
    ]);

    if ($element['country']['#access']) {
      // The country selection does not depend on other elements.
      $countries = static::cdekApi()->getCountries($request) ?: [];
      $country = isset($countries[$value['country']]) ? $value['country'] : '';

      $element['country']['#options'] = $countries;
      $element['country']['#value'] = $country;
      static::processNestedValue($element['country']);

      // If the country is not yet selected, block further selection.
      // Otherwise, add the country to the parameters.
      if ($element['country']['#value'] === '') {
        $is_value_locked = TRUE;
      }
      else {
        $request->setCountryId($value['country']);
      }
    }

    // Element to select the region.
    $element['region'] = $element['region'] ?? [];
    $element['region'] = static::processNestedElement($element['region'], [
      '#type' => 'select',
      '#title' => t('Region'),
      '#ajax' => $ajax_settings,
      '#access' => !$hide_region,
      '#weight' => 10,
    ]);

    if ($element['region']['#access']) {
      // The region selection may depend on the selected country.
      $regions = !$is_value_locked ? (static::cdekApi()->getRegions($request) ?: []) : [];
      $region = isset($regions[$value['region']]) ? $value['region'] : '';

      $element['region']['#options'] = $regions;
      $element['region']['#value'] = $region;
      static::processNestedValue($element['region']);

      // If the region is not yet selected, block further selection.
      // Otherwise, add the region to the parameters.
      if ($element['region']['#value'] === '') {
        $is_value_locked = TRUE;
      }
      else {
        $request->setRegionId($value['region']);
      }
    }

    // Element to select the city.
    $element['city'] = $element['city'] ?? [];
    $element['city'] = static::processNestedElement($element['city'], [
      '#type' => 'select',
      '#title' => t('City'),
      '#ajax' => $ajax_settings,
      '#access' => !$hide_city,
      '#weight' => 20,
    ]);

    if ($element['city']['#access']) {
      // The city selection may depend on the selected country and region.
      $cities = !$is_value_locked ? (static::cdekApi()->getCities($request) ?: []) : [];
      $city = isset($cities[$value['city']]) ? $value['city'] : '';

      $element['city']['#options'] = $cities;
      $element['city']['#value'] = $city;
      static::processNestedValue($element['city']);

      // If the city is not yet selected, block further selection.
      // Otherwise, add the city to the parameters.
      if ($element['city']['#value'] === '') {
        $is_value_locked = TRUE;
      }
      else {
        $request->setCityId($value['city']);
      }
    }

    // Element to select the point.
    $element['point'] = $element['point'] ?? [];
    $element['point'] = static::processNestedElement($element['point'], [
      '#type' => 'select',
      '#title' => t('Point'),
      '#weight' => 30,
    ]);

    // The point selection may depend on the selected country, region and city.
    $points = !$is_value_locked ? (static::cdekApi()->getPickupPoints($request) ?: []) : [];
    $points = array_column($points, 'Name', 'Code');
    $point = isset($points[$value['point']]) ? $value['point'] : '';

    $element['point']['#options'] = $points;
    $element['point']['#value'] = $point;
    static::processNestedValue($element['point']);

    // Prefix and suffix used for Ajax replacement.
    if ($element['country']['#access'] || $element['region']['#access'] || $element['city']['#access']) {
      $element['#prefix'] = '<div id="' . $ajax_wrapper_id . '">';
      $element['#suffix'] = '</div>';
    }
    return $element;
  }

  /**
   * After-build callback.
   *
   * @param array $element
   *   The element structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The element structure.
   */
  public static function afterBuildCdekSelect(array $element, FormStateInterface $form_state) {
    $element['#value'] = $element['point']['#value'];
    $form_state->setValueForElement($element, $element['#value']);
    return $element;
  }

  /**
   * Ajax callback to update element.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   The element to be rendered.
   */
  public static function ajaxUpdateCallback(array $form, FormStateInterface $form_state, Request $request) {
    // Get element parents from the request.
    $element_parents = explode('/', $request->query->get('element_parents'));
    // Sanitize element parents before using them.
    $element_parents = array_filter($element_parents, [Element::class, 'child']);
    // Retrieve the element to be rendered.
    return NestedArray::getValue($form, $element_parents);
  }

  /**
   * Processes the structure of a nested element.
   *
   * @param array $element
   *   The nested element to process.
   * @param array $default
   *   Default structure of the nested element.
   *
   * @return array
   *   The processed nested element.
   */
  protected static function processNestedElement(array $element, array $default) {
    return NestedArray::mergeDeepArray([$default, $element], TRUE);
  }

  /**
   * Processes the value of a nested element.
   *
   * @param array $element
   *   The nested element to process.
   */
  protected static function processNestedValue(array &$element) {
    if ($element['#type'] == 'select') {
      $element['#empty_value'] = '';
    }
    if ($element['#type'] == 'radios') {
      $element['#default_value'] = $element['#value'];
    }
  }

  /**
   * Resolves the request object.
   *
   * @param array $element
   *   The element structure.
   *
   * @return \CdekSDK\Requests\PvzListRequest
   *   An instance of the request object.
   */
  protected static function resolveRequest(array $element) {
    $request = $element['#cdek_request'];
    if ($request instanceof PvzListRequest) {
      return clone $request;
    }
    else {
      return new PvzListRequest();
    }
  }

  /**
   * Gets the cdek_api service.
   *
   * @return \Drupal\cdek_api\CdekApi
   *   The cdek_api service.
   */
  protected static function cdekApi() {
    return \Drupal::service('cdek_api');
  }

}
