<?php

namespace Drupal\cdek_api\Event;

/**
 * Defines events for the CDEK API module.
 */
final class CdekApiEvents {

  /**
   * Name of the event fired when the list of pickup points is loaded.
   *
   * @Event
   *
   * @see \Drupal\cdek_api\Event\PickupPointsEvent
   * @see \Drupal\cdek_api\CdekApi::getPickupPoints()
   */
  const PICKUP_POINTS = 'cdek_api.pickup_points';

}
