<?php

namespace Drupal\cdek_api\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Defines the pickup points event.
 *
 * @see \Drupal\cdek_api\Event\CdekApiEvents
 */
class PickupPointsEvent extends Event {

  /**
   * Array of pickup points keyed by code.
   *
   * @var \CdekSDK\Common\Pvz[]
   */
  protected $points;

  /**
   * Parameters that were used to get the list of pickup points.
   *
   * @var array
   */
  protected $params;

  /**
   * PickupPointsEvent constructor.
   *
   * @param \CdekSDK\Common\Pvz[] $points
   *   Array of pickup points keyed by code.
   * @param array $params
   *   Parameters that were used to get the list of pickup points.
   */
  public function __construct(array $points, array $params) {
    $this->points = $points;
    $this->params = $params;
  }

  /**
   * Gets the list of pickup points.
   *
   * @return \CdekSDK\Common\Pvz[]
   *   Array of pickup points keyed by code.
   */
  public function getPoints() {
    return $this->points;
  }

  /**
   * Gets the list of parameters.
   *
   * @return array
   *   Parameters that were used to get the list of pickup points.
   */
  public function getParams() {
    return $this->params;
  }

  /**
   * Sets the list of pickup points.
   *
   * @param \CdekSDK\Common\Pvz[] $points
   *   Array of pickup points keyed by code.
   *
   * @return $this
   */
  public function setPoints(array $points) {
    $this->points = $points;
    return $this;
  }

}
