<?php

namespace Drupal\cdek_api\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Component\Utility\UrlHelper;
use Drupal\cdek_api\CdekApi;
use CdekSDK\Requests\CalculationAuthorizedRequest;
use CdekSDK\Requests\CalculationRequest;
use CdekSDK\Requests\PvzListRequest;

/**
 * Controller routines for widget routes.
 */
class WidgetController extends ControllerBase {

  /**
   * Widget action ID; Returns the pickup points data.
   */
  const ACTION_PVZ = 'getPVZ';

  /**
   * Widget action ID; Returns translations for the widget.
   */
  const ACTION_LANG = 'getLang';

  /**
   * Widget action ID; Returns the city ID by its name.
   */
  const ACTION_CITY = 'getCity';

  /**
   * Widget action ID; Calculates shipping cost.
   */
  const ACTION_CALC = 'calc';

  /**
   * The cdek_api service.
   *
   * @var \Drupal\cdek_api\CdekApi
   */
  protected $cdekApi;

  /**
   * WidgetController constructor.
   *
   * @param \Drupal\cdek_api\CdekApi $cdek_api
   *   The cdek_api service.
   */
  public function __construct(CdekApi $cdek_api) {
    $this->cdekApi = $cdek_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cdek_api')
    );
  }

  /**
   * Returns JSON representing the response to the specified widget action.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function action(Request $request) {
    $action = $request->query->get('isdek_action');
    $data = $this->doAction($action, $request);
    return new JsonResponse($data);
  }

  /**
   * Performs a widget action.
   *
   * @param string $action
   *   Action to perform.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   The result of the action.
   */
  protected function doAction($action, Request $request) {
    $data = [];

    try {
      switch ($action) {
        case static::ACTION_PVZ:
          $data = $this->doActionPvz($request);
          break;

        case static::ACTION_LANG:
          $data = $this->doActionLang();
          break;

        case static::ACTION_CITY:
          $data = $this->doActionCity($request);
          break;

        case static::ACTION_CALC:
          $data = $this->doActionCalc($request);
          break;
      }
    }
    catch (WidgetActionException $e) {
      $data = ['error' => $e->getMessage()];
    }
    return $data;
  }

  /**
   * Performs a widget action.
   *
   * Returns the pickup points data.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   The result of the action.
   *
   * @throws \Drupal\cdek_api\Controller\WidgetActionException
   *   If the action fails.
   */
  protected function doActionPvz(Request $request) {
    $points_request = new PvzListRequest();
    $points_request->setType(PvzListRequest::TYPE_ALL);

    $lang = $request->query->get('lang', '');
    $country = $request->query->get('country', '');

    if ($lang !== '') {
      $points_request->setLanguage($lang);
    }
    if ($country !== '' && $country !== 'all') {
      $countries = $this->cdekApi->getCountries($points_request);
      $country_id = array_search($country, $countries);

      if ($country_id !== FALSE) {
        $points_request->setCountryId($country_id);
      }
    }

    $points = $this->cdekApi->getPickupPoints($points_request);
    if ($points === NULL) {
      throw new WidgetActionException('Failed to get the list of pickup points');
    }

    // Prepare a data structure.
    $data = array_fill_keys(['PVZ', 'CITY', 'REGIONS', 'CITYFULL'], []);
    foreach ($points as $code => $point) {
      $item = [
        'Name' => $point->Name,
        'WorkTime' => $point->WorkTime,
        'Address' => $point->Address,
        'Phone' => $point->Phone,
        'Note' => $point->Note,
        'cX' => $point->coordX,
        'cY' => $point->coordY,
        'Dressing' => $point->IsDressingRoom,
        'Cash' => $point->HaveCashless,
        'Postamat' => strtolower($point->Type) == 'postomat',
        'Station' => $point->NearestStation,
        'Site' => $point->Site,
        'Metro' => $point->MetroStation,
        'AddressComment' => $point->AddressComment,
        'CityCode' => $point->CityCode,
        'Picture' => [],
      ];

      if (!empty($point->WeightLimit)) {
        $item['WeightLim'] = [
          'MIN' => $point->WeightLimit->getWeightMin(),
          'MAX' => $point->WeightLimit->getWeightMax(),
        ];
      }
      foreach ($point->OfficeImages as $office_image) {
        if (UrlHelper::isValid($office_image->getUrl(), TRUE)) {
          $item['Picture'][] = $office_image->getUrl();
        }
      }
      // Add item to the result data array.
      $data['PVZ'][$point->CityCode][$code] = $item;

      // Add additional information.
      if (!isset($data['CITY'][$point->CityCode])) {
        $city = trim(strtok($point->City, ',('));
        $country = $point->CountryName;
        $region = $point->RegionName;

        $data['CITY'][$point->CityCode] = $city;
        $data['CITYREG'][$point->CityCode] = $point->RegionCode;
        $data['REGIONSMAP'][$point->RegionCode][] = $point->CityCode;
        $data['CITYFULL'][$point->CityCode] = "$country $region $city";
        $data['REGIONS'][$point->CityCode] = "$region, $country";
      }
    }
    return ['pvz' => $data];
  }

  /**
   * Performs a widget action.
   *
   * Returns translations for the widget.
   *
   * @return array
   *   The result of the action.
   */
  protected function doActionLang() {
    $options = ['context' => 'CDEK Widget'];

    $lang = [
      'COURIER' => $this->t('Courier', [], $options),
      'PICKUP' => $this->t('Pickup', [], $options),
      'DAY' => $this->t('days', [], $options),
      'RUB' => $this->t('RUB', [], $options),
      'CITYSEARCH' => $this->t('Search for a city', [], $options),
      'PVZ' => $this->t('Points of self-delivery', [], $options),
      'COUNTING' => $this->t('Calculation', [], $options),
      'NO_AVAIL' => $this->t('No shipping methods available', [], $options),
      'CHOOSE_TYPE_AVAIL' => $this->t('Choose a shipping method', [], $options),
      'CHOOSE_OTHER_CITY' => $this->t('Choose another location', [], $options),
      'TYPE_ADDRESS' => $this->t('Specify address', [], $options),
      'TYPE_ADDRESS_HERE' => $this->t('Specify shipping address', [], $options),
      'L_ADDRESS' => $this->t('Address of self-delivery', [], $options),
      'L_TIME' => $this->t('Working hours', [], $options),
      'L_WAY' => $this->t('How to get to us', [], $options),
      'L_CHOOSE' => $this->t('Choose', [], $options),
      'H_LIST' => $this->t('List of self-delivery', [], $options),
      'H_PROFILE' => $this->t('Shipping method', [], $options),
      'H_CASH' => $this->t('Payment by card', [], $options),
      'H_DRESS' => $this->t('Dressing room', [], $options),
      'H_POSTAMAT' => $this->t('Postamats CDEK', [], $options),
      'H_SUPPORT' => $this->t('Support', [], $options),
      'H_QUESTIONS' => $this->t('If you have any questions,<br> you can ask them to our specialists', [], $options),
      'ADDRESS_WRONG' => $this->t('Impossible to define address. Please, recheck the address.', [], $options),
      'ADDRESS_ANOTHER' => $this->t('Read the new terms and conditions.', [], $options),
    ];
    return ['LANG' => $lang];
  }

  /**
   * Performs a widget action.
   *
   * Returns the city ID by its name.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   The result of the action.
   *
   * @throws \Drupal\cdek_api\Controller\WidgetActionException
   *   If the action fails.
   */
  protected function doActionCity(Request $request) {
    // TODO: Process 'address' parameter.
    if (($city = $request->query->get('city', '')) === '') {
      throw new WidgetActionException('No city to search given');
    }
    if (($cities = $this->cdekApi->getCities()) === NULL) {
      throw new WidgetActionException('Failed to get the list of cities');
    }
    if (($id = $this->arraySearch($city, $cities)) === FALSE) {
      throw new WidgetActionException('No city found');
    }
    return ['id' => $id];
  }

  /**
   * Performs a widget action.
   *
   * Calculates shipping cost.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return array
   *   The result of the action.
   *
   * @throws \Drupal\cdek_api\Controller\WidgetActionException
   *   If the action fails.
   */
  protected function doActionCalc(Request $request) {
    $widget_id = $request->attributes->get('widget_id');
    $shipment = $request->query->get('shipment');

    if (!isset($shipment['type']) || $shipment['type'] === '') {
      throw new WidgetActionException('Type not specified');
    }
    if (!isset($shipment['cityFromId']) || $shipment['cityFromId'] === '') {
      throw new WidgetActionException('Sender city not specified');
    }
    if (!isset($shipment['cityToId']) || $shipment['cityToId'] === '') {
      throw new WidgetActionException('Receiver city not specified');
    }
    if (empty($shipment['goods']) || !is_array($shipment['goods'])) {
      throw new WidgetActionException('Goods not specified');
    }

    $calc_request = $this->cdekApi->hasCredentials() ? new CalculationAuthorizedRequest() : new CalculationRequest();
    $calc_request->setSenderCityId($shipment['cityFromId']);
    $calc_request->setReceiverCityId($shipment['cityToId']);

    if (!empty($widget_id)) {
      $cid = 'tariff_list:' . $widget_id;
      $cache = $this->cache('cdek_api')->get($cid);

      if (isset($cache->data[$shipment['type']])) {
        $tariff_list = array_values($cache->data[$shipment['type']]);
      }
    }

    if (!empty($tariff_list)) {
      foreach ($tariff_list as $priority => $tariff_id) {
        $calc_request->addTariffToList($tariff_id, $priority + 1);
      }
    }
    else {
      $calc_request->setTariffId(1);
    }

    foreach ($shipment['goods'] as $good) {
      $calc_request->addPackage($good);
    }

    try {
      $response = $this->cdekApi->getCdekClient()
        ->sendCalculationRequest($calc_request);
    }
    catch (\Exception $e) {
      throw new WidgetActionException('Failed to calculate');
    }
    if ($response->hasErrors()) {
      throw new WidgetActionException('Failed to calculate');
    }

    $delivery_date_min = $response->getDeliveryDateMin();
    if ($delivery_date_min instanceof \DateTimeImmutable) {
      $delivery_date_min = $delivery_date_min->format('Y-m-d');
    }
    $delivery_date_max = $response->getDeliveryDateMax();
    if ($delivery_date_max instanceof \DateTimeImmutable) {
      $delivery_date_max = $delivery_date_max->format('Y-m-d');
    }

    $result = [
      'price' => $response->getPrice(),
      'deliveryPeriodMin' => $response->getDeliveryPeriodMin(),
      'deliveryPeriodMax' => $response->getDeliveryPeriodMax(),
      'deliveryDateMin' => $delivery_date_min,
      'deliveryDateMax' => $delivery_date_max,
      'tariffId' => $response->getTariffId(),
    ];

    return [
      'type' => $shipment['type'],
      'timestamp' => $shipment['timestamp'] ?? '',
      'result' => $result,
    ];
  }

  /**
   * Case in-sensitive array_search() with partial matches.
   *
   * @param string $needle
   *   The string to search for.
   * @param array $haystack
   *   The array to search in.
   *
   * @return mixed
   *   The key for needle if it is found in the array, FALSE otherwise.
   */
  protected function arraySearch($needle, array $haystack) {
    foreach ($haystack as $key => $value) {
      if (mb_stripos($value, $needle) === 0) {
        return $key;
      }
    }
    return FALSE;
  }

}
