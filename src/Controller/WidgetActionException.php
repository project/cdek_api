<?php

namespace Drupal\cdek_api\Controller;

/**
 * Exception that indicates an error in the action of the pickup points widget.
 */
class WidgetActionException extends \RuntimeException {}
